package com.qbabe.expenses;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.qbabe.expenses.controller.ExpenseController;

@EnableAutoConfiguration
@Configuration
@ComponentScan
@PropertySource("classpath:application.properties")
public class Server {

	public static void main(String[] args) {
		SpringApplication.run(Server.class, args);
	}

}

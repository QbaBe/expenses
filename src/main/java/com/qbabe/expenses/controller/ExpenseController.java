package com.qbabe.expenses.controller;

import static ch.lambdaj.Lambda.convert;

import java.math.BigDecimal;
import java.security.Principal;
import java.util.List;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qbabe.expense.converter.ExpenseConverter;
import com.qbabe.expenses.domain.Expense;
import com.qbabe.expenses.domain.User;
import com.qbabe.expenses.dto.ExpenseDTO;
import com.qbabe.expenses.repository.ExpenseRepository;
import com.qbabe.expenses.repository.UserRepository;

@Controller
@RequestMapping("/expense")
public class ExpenseController {

	@Autowired
	private ExpenseRepository expenseRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@RequestMapping(method=RequestMethod.GET)
	@ResponseBody
	public List<ExpenseDTO> getAll() {
		return convert(expenseRepository.findAll(), new ExpenseConverter());
	}
	
	@RequestMapping("/{id}")
	@ResponseBody
	public ExpenseDTO getExpense(@PathVariable long id) {
		return convert(expenseRepository.findOne(id), new ExpenseConverter()).get(0);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	@ResponseBody
	public String add(@RequestBody ExpenseDTO expenseDto, Principal principal) {
		User user = userRepository.findByUsername(principal.getName());
		Expense expense = new Expense();
		expense.setName(expenseDto.getName());
		expense.setCost(BigDecimal.valueOf(expenseDto.getCost()));
		expense.setDate(LocalDate.parse(expenseDto.getDate()));
		expense.setUser(user);
		expense.addCategory(expenseDto.getCategory());
		expenseRepository.save(expense);
		return "Expense created: " + expense.getName();
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	@ResponseBody
	public String delete(@PathVariable long id) {
		expenseRepository.delete(id);
		return "Expense deleted";
	}
}

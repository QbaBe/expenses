package com.qbabe.expenses.controller;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qbabe.expenses.domain.Category;
import com.qbabe.expenses.domain.User;
import com.qbabe.expenses.repository.CategoryRepository;
import com.qbabe.expenses.repository.UserRepository;

@Controller
@RequestMapping("/category")
public class CategoryController {

	@Autowired
	private CategoryRepository categoryRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@RequestMapping(method=RequestMethod.GET)
	@ResponseBody
	public List<Category> getAll() {
		return categoryRepository.findAll();
	}
	
	@RequestMapping("/{id}")
	@ResponseBody
	public Category getCategory(@PathVariable long id) {
		return categoryRepository.findOne(id);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	@ResponseBody
	public String add(@RequestBody String name, Principal principal) {
		User user = userRepository.findByUsername(principal.getName());
		Category category = new Category();
		category.setName(name);
		category.setUser(user);
		categoryRepository.save(category);
		return "Category created: " + category.getName();
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	@ResponseBody
	public String delete(@PathVariable long id) {
		categoryRepository.delete(id);
		return "Category deleted";
	}
}

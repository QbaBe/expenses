package com.qbabe.expenses.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.qbabe.expenses.domain.User;

public interface UserRepository extends JpaRepository<User, Long> {

	User findByUsername(String usernamme);
}

package com.qbabe.expenses.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.qbabe.expenses.domain.Category;

public interface CategoryRepository extends JpaRepository<Category, Long> {

	Category findByName(String name);
}

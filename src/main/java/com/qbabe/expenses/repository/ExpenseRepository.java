package com.qbabe.expenses.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.qbabe.expenses.domain.Expense;

public interface ExpenseRepository extends JpaRepository<Expense, Long>{
	
}

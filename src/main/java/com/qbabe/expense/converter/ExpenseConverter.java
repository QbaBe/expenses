package com.qbabe.expense.converter;

import com.qbabe.expenses.domain.Expense;
import com.qbabe.expenses.dto.ExpenseDTO;

import ch.lambdaj.function.convert.Converter;

public class ExpenseConverter implements Converter<Expense, ExpenseDTO> {
	
	@Override
	public ExpenseDTO convert(Expense expense) {
		ExpenseDTO dto = new ExpenseDTO();
		dto.setId(expense.getId());
		dto.setName(expense.getName());
		dto.setCost(expense.getCost().doubleValue());
		dto.setDate(expense.getDate().toString());
		return dto;
	}
}

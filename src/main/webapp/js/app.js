var myApp = angular.module('myApp', ['restService', 'ui.bootstrap', 'ngRoute']);

myApp.controller('MyController', function ($scope, $modal, Expense, Category) {

	getExpenses();
	getCategories();
	
	$scope.createNewExpense = function() {
		var modalInstance = $modal.open({
      		templateUrl: '/template/expenseSave.html',
      		controller: 'ModalController'
    	});

    	modalInstance.result.then(function () {
      		getExpenses();
    	});
	}

	$scope.deleteExpense = function(expenseId) {
		if(expenseId) {
			Expense.delete({id:expenseId}, function() {
				getExpenses();
			});
		} else {
			alert("Expense Id is empty");
		}	
	}

	function getExpenses() {
		$scope.expenses = Expense.query();
	}

	function getCategories() {
		$scope.categories = Category.query();
	}

});

myApp.controller('ModalController', function ($scope, $modalInstance, Expense, Category) {

	$('#expenseDate').datepicker({
   		format: "yyyy-mm-dd",
   		autoclose: true
    }); 

	getCategories();

	function getCategories() {
		$scope.categories = Category.query();
	}

	function getExpenses() {
		$scope.expenses = Expense.query();
	}

	$scope.submitExpense = function() {
		var newExpense = new Expense({ 
			name : $scope.newExpense.name,
			cost : $scope.newExpense.cost,
			date : $scope.newExpense.date,
			category : $scope.newExpense.category
		});
		if(newExpense.name && newExpense.cost && newExpense.date) {
			newExpense.$save(function(){
				$modalInstance.close();
			});
		} else {
			alert("Please enter all fields");
		}	
	}

	$scope.cancelExpense = function() {
		$modalInstance.dismiss('cancel');
	}
});

myApp.config(function($routeProvider){
	$routeProvider
		.when("/expenses", { templateUrl: "/template/expenseAll.html" })
		.when("/categories", { templateUrl: "/template/categoryAll.html" })
		.when("/about", { templateUrl: "/template/about.html" })
		.otherwise( { redirectTo: "/expenses" });
})

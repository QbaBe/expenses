var myDialog = angular.module('myDialog', ["restService"]);

myDialog.factory("MyDialog", function() {
	return {
		showSaveExpenseDialog : saveExpenseDialog,
		showSaveCategoryDialog : saveCategoryDialog,
		showAboutAlert : aboutAlert
	};
});

function saveExpenseDialog() {
	bootbox.dialog({
            title: "Save Expense",
            message: '<div ng-controller="MyController">  ' +
                    '<form id="newExpenseForm" ng-submit="submitExpense()"> ' +
						'<input id="expenseName" type="text" class="form-control" ng-model="newExpense.name" placeholder="Name"/>' +
						'<input id="expenseCost" type="number" class="form-control" ng-model="newExpense.cost" placeholder="Cost"/>' +
						'<input id="expenseDate" type="text" class="form-control" ng-model="newExpense.date" placeholder="Date"/>' +
						'<select id="expenseCategory" class="form-control" ng-model="newExpense.category" ng-options="category.name for category in categories"></select>' +
						'<button id="expenseSave" type="submit" class="btn btn-primary">Save</button>' +
					'</form>' +
            	    '</div>',
            buttons: {
                success: {
                    label: "Save",
                    className: "btn-success",
                    callback: function () {
                        alert("Saved");
                    }
                }
            }
        }
   );
}

function saveCategoryDialog() {
	bootbox.dialog({
            title: "Save Category",
            message: '<div class="row">  ' +
                    '<div class="col-md-12"> ' +
					'<input id="categoryName" type="text" class="form-control" ng-model="newCategory.name" placeholder="Name"/>' +
            	    '</div>  </div>',
            buttons: {
                success: {
                    label: "Save",
                    className: "btn-success",
                    callback: function () {
                        alert("Saved");
                    }
                }
            }
        }
   );
}

function aboutAlert() {
	bootbox.alert('ExpenseApp made by QbaBe');
}
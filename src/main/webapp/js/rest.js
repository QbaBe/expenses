var restService = angular.module('restService', ["ngResource"]);

restService.factory("Expense", function($resource) {
	return $resource("/expense/:id", {id: "@id" }); 
});

restService.factory("Category", function($resource) {
	return $resource("/category/:id", {id: "@id" }); 
});